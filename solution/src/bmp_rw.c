#include "bmp_rw.h"
#include <stdbool.h>
#include <stdio.h>

enum opening_state open_file(bool mode, char *path, FILE **picture) {
    if (!mode) {
        *picture = fopen(path, "rb"); // open for reading
        if (!*picture) {
            return FAILED_OPEN;
        }
    } else {
        *picture = fopen(path, "wb"); // open for writting
        if (!*picture) {
            return FAILED_OPEN;
        }
    }
    return SUCCESS_OPEN;
}


enum closing_state close_file(FILE **picture) {
    if (fclose(*picture) == EOF) {
        return FAILED_CLOSE;
    } else {
        return SUCCESS_CLOSE;
    }
}

