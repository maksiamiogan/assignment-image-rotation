#include "outside.h"
#include "bmp_rw.h"
#include "cover.h"
#include "transform.h"
#include <stdio.h>
#include <stdlib.h>


int main(int argc, char **argv) {
    // argc - function name, input picture path, output picture path
    if (argc != 3) {
        fprintf(stderr, "Wrong amount of input parameters\n");
        return 1;
    }
    struct image img = read_from_path(argv[1]);
    // rotating the image
    struct image rotated_img = rotate(&img);

    if(!write_by_path(argv[2],rotated_img)){
        fprintf(stderr,"Some problems with writing image to file");
        free(img.data);
        free(rotated_img.data);
        return 1;
    }
    // free memory
    free(img.data);
    free(rotated_img.data);

    return 0;
}
