//
// Created by maksi on 29.12.2021.
//
#include <stdio.h>
#include <stdlib.h>

#include "inside.h"

size_t rotated_coord(size_t i, size_t j, size_t width) {
    return (i + (j * width));
}

size_t basic_coord(size_t i, size_t j, size_t width, size_t height) {
    return (j + ((width - i - 1) * height));
}

struct image rotate(struct image const *img) {
    struct image rotated_img = {img->height, img->width,
                                (struct pixel *) malloc(sizeof(struct pixel) * img->width * img->height)
    };
    for (size_t i = 0; i < rotated_img.width; i++) {
        for (size_t k = 0; k < rotated_img.height; k++) {
            rotated_img.data[rotated_coord(i, k, rotated_img.width)] = (img->data)[basic_coord(i, k, rotated_img.width,
                                                                                               rotated_img.height)];
        }
    }

    return rotated_img;
}
