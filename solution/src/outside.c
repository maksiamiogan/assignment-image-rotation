//
// Created by maksi on 30.12.2021.
//

//
// Created by maksi on 27.11.2021.
//

#include "inside.h"
#include "outside.h"
#include <assert.h>
#include  <stdint.h>
#include <stdlib.h>

#define bfT 0x4D42
#define bfR 0
#define biS 40
#define biP 1
#define bBC 24
#define biC 0
#define biXPPM 0
#define biYPPM 0
#define bcU 0
#define bcI 0

#pragma pack(push, 1)
struct bmp_header {
    uint16_t bfType; //	Сигнатура "BM" 0x4D42
    uint32_t bfileSize; // Размер файла
    uint32_t bfReserved; //	Зарезервировано
    uint32_t bOffBits; // 	Смещение изображения от начала файла
    uint32_t biSize; // Длина заголовка. это размер только BITMAPINFOHEADER . Это 40 байт.
    uint32_t biWidth; // 	Ширина изображения, точки
    uint32_t biHeight;// Высота изображения, точки
    uint16_t biPlanes; // 	Число плоскостей
    uint16_t biBitCount; // Глубина цвета, бит на точку
    uint32_t biCompression; // Тип компрессии (0 - несжатое изображение)
    uint32_t biSizeImage; // 	Размер изображения, байт
    uint32_t biXPelsPerMeter; // Горизонтальное разрешение, точки на метр
    uint32_t biYPelsPerMeter; // Вертикальное разрешение, точки на метр
    uint32_t biClrUsed; // Число используемых цветов (0 - максимально возможное для данной глубины цвета)
    uint32_t biClrImportant; // 	Число основных цветов
};
#pragma pack(pop)

enum read_status from_bmp(FILE *in, struct image *img) {
    struct bmp_header bmp_h;
    if (!in) {
        return READ_FAILED_FILE;
    }
    if (!img) {
        return READ_FAILED_IMAGE;
    }

    // читаем header
    fread(&bmp_h, sizeof(struct bmp_header), 1, in);
    // проверяем header
    if (bmp_h.biPlanes != biP || bmp_h.bfReserved != bfR) {
        return READ_FAILED;
    }
    if (bmp_h.biSize != biS) {
        return READ_INVALID_INFO_HEADER;
    }
    if (bmp_h.biBitCount != bBC) {
        return READ_INVALID_BITS;
    }
    if (bmp_h.bfType != bfT) {
        return READ_INVALID_SIGNATURE;
    }
    // найдем padding
    int padding = (int) (bmp_h.biWidth % 4);
    // занесем данные в структуру image
    img->width = bmp_h.biWidth;
    img->height = bmp_h.biHeight;
    img->data = (struct pixel *) malloc(sizeof(struct pixel) * img->height * img->width);
    // читаем изображение
    for (int i = 0; i < bmp_h.biHeight; i++) {
        assert(fread(&(img->data[i * bmp_h.biWidth]), sizeof(struct pixel), bmp_h.biWidth, in) == bmp_h.biWidth);
        assert(!fseek(in, padding, SEEK_CUR));
    }
    return READ_OK;
}


enum write_status to_bmp(FILE *out, struct image const *img) {
    if (out == NULL) {
        return WRITE_FAILED_FILE;
    }
    if (img == NULL) {
        return WRITE_FAILED_IMAGE;
    }
    int padding = (int) img->width % 4;
    // заполним header
    struct bmp_header bmp_h = {
            bfT,
            img->width * img->height * sizeof(struct pixel) + sizeof(struct bmp_header) + padding * img->height,
            bfR,
            sizeof(struct bmp_header),
            biS,
            img->width,
            img->height,
            biP,
            bBC,
            biC,
            padding * img->height + sizeof(struct pixel) * img->height * img->width,
            biXPPM,
            biYPPM,
            bcU,
            bcI,
    };
    // записываем header
    assert(fwrite(&bmp_h, sizeof(struct bmp_header), 1, out));
    // записываем картинку
    for (int i = 0; i < img->height; i++) {
        assert(fwrite((char *) img->data + i * img->width * 3, 3, img->width, out));
        assert(!fseek(out, padding, SEEK_CUR));
    }
    return WRITE_OK;
}
