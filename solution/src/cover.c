//
// Created by maksi on 30.01.2022.
//

#include "outside.h"
#include "bmp_rw.h"
#include "inside.h"
#include <stdlib.h>
#define PIC_NULL (struct image){.height=0,.width=0, .data=NULL};


struct image read_from_path(char* path) {
    FILE *in_pic = NULL;

    // open input file
    switch (open_file(0, path, &in_pic)) {
        case FAILED_OPEN:
            fprintf(stderr, "Failed while opening input file.\n");
            return PIC_NULL;
        case SUCCESS_OPEN:
            break;
    }
    struct image img;
    enum read_status r_s = from_bmp(in_pic, &img);

    if(r_s!=READ_OK){
        close_file(&in_pic);
    }

    // reading from file to image structure
    switch (r_s) {
        case READ_FAILED_IMAGE:
            fprintf(stderr, "Image is null\n");
        case READ_FAILED_FILE:
            fprintf(stderr, "File is null\n");
        case READ_INVALID_SIGNATURE:
            fprintf(stderr, "Invalid Signature\n");
            return PIC_NULL;
        case READ_INVALID_INFO_HEADER:
            fprintf(stderr, "Invalid INFO Header\n");
            return PIC_NULL;
        case READ_FAILED:
            fprintf(stderr, "Read failed\n");
            return PIC_NULL;
        case READ_INVALID_BITS:
            fprintf(stderr, "Invalid Bits\n");
            return PIC_NULL;
        case READ_OK:
            fprintf(stderr, "Read - ok\n");
            break;
    }

    switch (close_file(&in_pic)) {
        case FAILED_CLOSE:
            fprintf(stderr, "Fail while closing input file\n");
            return PIC_NULL;
        case SUCCESS_CLOSE:
            break;
    }

    return img;
}

bool write_by_path(char * path, const struct image pict){
    FILE *out_pic = NULL;
    switch (open_file(1, path, &out_pic)) {
        case FAILED_OPEN:
            fprintf(stderr, "Failed while opening output file.\n");
            return false;
        case SUCCESS_OPEN:
            break;
    }
    // write rotated image into output file
    switch (to_bmp(out_pic, &pict)) {
        case WRITE_FAILED_FILE:
            fprintf(stderr, "file = NULL\n");
            return false;
        case WRITE_FAILED_IMAGE:
            fprintf(stderr, "Image = NULL\n");
            return false;
        case WRITE_ERROR:
            fprintf(stderr, "Write error\n");
            return false;
        case WRITE_OK:
            fprintf(stderr, "Write - ok\n");
            break;
    }
    // closing  output file


    switch (close_file(&out_pic)) {
        case FAILED_CLOSE:
            fprintf(stderr, "Fail while closing output file\n");
            return false;
        case SUCCESS_CLOSE:
            break;
    }
    return true;
}
