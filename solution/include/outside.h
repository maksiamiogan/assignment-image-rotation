//
// Created by maksi on 17.12.2021.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_OUTSIDE_H
#define ASSIGNMENT_IMAGE_ROTATION_OUTSIDE_H
#include "inside.h"
#include <stdint.h>
#include <stdio.h>

struct bmp_header;
enum read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_INFO_HEADER,
    READ_FAILED_FILE,
    READ_FAILED_IMAGE,
    READ_FAILED
};
enum write_status {
    WRITE_OK = 0,
    WRITE_ERROR,
    WRITE_FAILED_FILE,
    WRITE_FAILED_IMAGE
};

enum read_status from_bmp(FILE *in, struct image *img);

enum write_status to_bmp(FILE *out, const struct image *img);

#endif //ASSIGNMENT_IMAGE_ROTATION_OUTSIDE_H
