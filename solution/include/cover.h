//
// Created by maksi on 30.01.2022.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_COVER_H
#define ASSIGNMENT_IMAGE_ROTATION_COVER_H
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
bool write_by_path(char * path, const struct image pict);
struct image read_from_path(char* path);
#endif //ASSIGNMENT_IMAGE_ROTATION_COVER_H
