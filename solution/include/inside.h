//
// Created by maksi on 30.12.2021.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_INSIDE_H
#define ASSIGNMENT_IMAGE_ROTATION_INSIDE_H

#include <stdint.h>
#include <stdio.h>

#pragma pack(push, 1)
struct pixel {
    uint8_t b, g, r;
};
#pragma pack(pop)

struct image {
    uint64_t width, height;
    struct pixel *data;
};

#endif //ASSIGNMENT_IMAGE_ROTATION_INSIDE_H
