//
// Created by maksi on 29.12.2021.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_TRANSFORM_H
#define ASSIGNMENT_IMAGE_ROTATION_TRANSFORM_H

struct image rotate(struct image const *img);

#endif //ASSIGNMENT_IMAGE_ROTATION_TRANSFORM_H
