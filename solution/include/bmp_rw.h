//
// Created by maksi on 26.12.2021.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_BMP_RW_H
#define ASSIGNMENT_IMAGE_ROTATION_BMP_RW_H

#include <stdbool.h>
#include <stdio.h>

enum opening_state {
    SUCCESS_OPEN,
    FAILED_OPEN
};
enum closing_state {
    SUCCESS_CLOSE,
    FAILED_CLOSE
};

enum opening_state open_file(bool mode, char *path, FILE **picture);

enum closing_state close_file(FILE **picture);

#endif //ASSIGNMENT_IMAGE_ROTATION_BMP_RW_H
